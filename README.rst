WDM Chrome Extension
============================

This repository contains Chrome extension code for WDM (Washroom Door Monitor) project.

How do start on developing chrome extensions?
_______________________________________________

Visit this Chrome developers url for getting started on chrome extensions: https://developer.chrome.com/extensions

How can I add this WDM Chrome extension in my browser?
_________________________________________________________

To add this extension "wdm-chrom-extension" follow these steps:

1. Download/Checkout this repo **wdm-chrom-extension** to your local machine where you want to install it in your Chrome browser.
2. Open your Chrome browser and go to **Chrome** ``->`` **Window** ``->`` **Extensions**
3. In the header of **Extensions** page find the check box **Developer mode** on right side and enable it.
4. Now you will see two new buttons on with the label **Load unpacked extension**.
5. Click on the button **Load unpacked extension** and provide it the checkout/downloaded folder **wdm-chrom-extension** path.
6. After this extension is added in your Chrome browser you will be able to see a new button with the title ``"WDM Extension"`` in the extensions bar (top left).
7. Click on the ``"WDM Extension"`` and you will see a popup UI with the title ``"Arbisoft washrooms statuses"``.
8. Until the popup is open it will automatically update it after a fixed interval (default timeout set at 2 sec).

Does WDM Chrome extension stores any data on my system?
_________________________________________________________

This extension just dynamically displays the WDM responses and do not store any cookie or other content on your system.
This extension only fetches data from the WDM server and do not post/track any information to any server.
