$(document).ready(function () {
    // Flask web-server IP's on different networks
    var WDM_SERVER_BASE_URLS = [
        "http://125.209.110.74:9898/",
        "http://172.16.13.226:9898/"
    ];

    // Flask web-server API url for getting washroom door statuses from each
    // floor at Arbisoft
    var API_URL = "washroom_door_monitor/api/v1/";

    var REQUEST_INTERVAL = 1000;  // WDM data fetch interval value in milli seconds: 1000 = 1 second

    // Initially hide all cards in WdmPopupUI
    hideAllCards();

    // Ping the WDM servers and start updating the UI if any of the server is responding
    WDM_SERVER_BASE_URLS.forEach(function(wdmBaseUrl, index, array){
        $.ajax({
            url: wdmBaseUrl
        }).done(function (result) {
            fetchWdmApiAndUpdatePageUiAtRegularIntervals(wdmBaseUrl + API_URL);
        })
    });

    function fetchWdmApiAndUpdatePageUiAtRegularIntervals(wdmApiUrl) {
        $.ajax({
            type: 'GET',
            url: wdmApiUrl,
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }).done(function (data) {
            updateWdmPopupUI(data);
        }).fail(function (data) {
            hideAllCardsAndShowErrorCard();
        }).always(function (data) {
            // Schedule the next call for updating UI
            console.log(REQUEST_INTERVAL);
            setTimeout(fetchWdmApiAndUpdatePageUiAtRegularIntervals, REQUEST_INTERVAL, wdmApiUrl);
        });
    }
});

function updateWdmPopupUI(wdm_api_data) {
    var floor0Card = $('div.card.floor-0');
    var floor1Card = $('div.card.floor-1');
    var floor2Card = $('div.card.floor-2');

    resetAllCards();
    hideAllCards();

    if (wdm_api_data.hasOwnProperty('floor_0')) {
        setFloorCardData(floor0Card, wdm_api_data['floor_0']);
    }

    if (wdm_api_data.hasOwnProperty('floor_1')) {
        setFloorCardData(floor1Card, wdm_api_data['floor_1']);
    }

    if (wdm_api_data.hasOwnProperty('floor_2')) {
        setFloorCardData(floor2Card, wdm_api_data['floor_2']);
    }
}

function setFloorCardData(floorCardElement, floorCardData) {
    var doorsMap = ['door_a', 'door_b', 'door_c', 'door_d'];
    var cardDoorStatuses, cardDoorTimeSpans, doorKey, doorStatus;

    if (floorCardData['success']) {
        cardDoorStatuses = floorCardElement.find('ul.door-statuses').children('li.door_status');
        cardDoorTimeSpans = floorCardElement.find('ul.occupancy-time').children('li').find('span.time');

        if (floorCardData['name'].length>1) {
            floorCardElement.find('div.card-title').text(floorCardData['name'])
        }
        for (var i = 0; i < doorsMap.length; ++i) {
            doorKey = doorsMap[i];
            if (floorCardData.hasOwnProperty(doorKey)) {
                doorStatus = floorCardData[doorKey];
                if (doorStatus['door_open']) {
                    $(cardDoorStatuses[i]).removeClass().addClass('door_status open');
                    $(cardDoorTimeSpans[i]).text('00:00');
                } else {
                    $(cardDoorStatuses[i]).removeClass().addClass('door_status closed');
                    $(cardDoorTimeSpans[i]).text(doorStatus['occupancy_time']);
                }
            }
        }
        floorCardElement.show();
    } else {
        // Hide floor card
        floorCardElement.hide();
    }
}

function resetAllCards() {
    var floor0Card = $('div.card.floor-0');
    var floor1Card = $('div.card.floor-1');
    var floor2Card = $('div.card.floor-2');

    floor0Card.find('ul.door-statuses').children('li.door_status').removeClass().addClass('door_status unavailable');
    floor0Card.find('ul.occupancy-time').children('li').find('span.time').text('00:00');
    floor1Card.find('ul.door-statuses').children('li.door_status').removeClass().addClass('door_status unavailable');
    floor1Card.find('ul.occupancy-time').children('li').find('span.time').text('00:00');
    floor2Card.find('ul.door-statuses').children('li.door_status').removeClass().addClass('door_status unavailable');
    floor2Card.find('ul.occupancy-time').children('li').find('span.time').text('00:00');
}

function hideAllCardsAndShowErrorCard() {
    var errorCard = $('div.card.error');

    hideAllCards();
    errorCard.show();
}

function hideAllCards() {
    var floor0Card = $('div.card.floor-0');
    var floor1Card = $('div.card.floor-1');
    var floor2Card = $('div.card.floor-2');
    var errorCard = $('div.card.error');

    floor0Card.hide();
    floor1Card.hide();
    floor2Card.hide();
    errorCard.hide();
}
